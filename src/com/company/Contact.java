package com.company;

import java.util.*;

public class Contact {
    private String name;
    private String surname;
    private Date birthday;
    private Date create;
    private ArrayList<Phone> phones = new ArrayList<>();
    private ArrayList<Email> emails = new ArrayList<>();


    Contact(String name, String surname) {
        this.name = name;
        this.surname = surname;
        this.create = new Date();
    }

    public void setName(String name){ this.name = name; }
    public String getName(){ return this.name; }

    public void setSurname(String surname){ this.surname = surname; }
    public String getSurname(){ return this.surname; }

    public void addPhone(Phone phone){ this.phones.add(phone); }
    public void addEmail(Email email){ this.emails.add(email); }

}
